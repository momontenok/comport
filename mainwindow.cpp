#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    serial = new QSerialPort(this); //AbstractSerial
    serial->setPortName("com1"); // порт и указатели на параметры
    serial->setBaudRate(QSerialPort::Baud9600);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    serial->open(QIODevice::ReadWrite); //открыть порт
    serial->write("hello");//запись данных

    connect(serial, SIGNAL(readyRead()), this, SLOT(serialReceive())); // соединение чтения-приема

    // получение списка доступных портов в системе с помощью QSerialPortInfo
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        QSerialPort port;
        port.setPort(info);
        if(port.open(QIODevice::ReadWrite))
        {
            qDebug() << "title: " + info.portName() + " " + info.description() + info.manufacturer();
        }
    }

}

MainWindow::~MainWindow()
{
    delete ui;

    serial->close();
    delete serial;
}

void MainWindow::serialReceive()
{
    QByteArray ba; // массив байт
    ba = serial->readAll(); // считывание данных
    ui->label->setText(ba.toHex()); //перевод в hex
}
